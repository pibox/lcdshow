# ---------------------------------------------------------------
# Build $(PK)
# ---------------------------------------------------------------

root-verify:
	@if [ $(UID) -ne 0 ]; then \
		$(MSG11) "You must be root." $(EMSG); \
		exit 1; \
	fi

opkg-verify:
	@if [ "$(OPKG_DIR)" = "" ] || [ ! -f $(OPKG_DIR)/opkg-build ]; then \
		$(MSG11) "Can't find opkg-build.  Try setting OPKG= to the directory it lives in." $(EMSG); \
		exit 1; \
	fi

build-verify:
	@if [ "$(CROSS_COMPILER)" = "" ] || [ ! -f $(CROSS_COMPILER) ]; then \
		$(MSG11) "Can't find cross toolchain.  Try setting XI= on the command line." $(EMSG); \
		exit 1; \
	fi
	@if [ "$(SD)" = "" ] || [ ! -d $(SD) ]; then \
		$(MSG11) "Can't find staging tree.  Try setting SD= on the command line." $(EMSG); \
		exit 1; \
	fi

# Retrieve package
$(PK_T)-get: .$(PK_T)-get

.$(PK_T)-get: 
	@if [ ! -d $(ARCDIR)/$(PK_ARCHIVE) ]; then \
		$(MSG) "================================================================"; \
		$(MSG2) "Retrieving files" $(EMSG); \
		$(MSG) "================================================================"; \
		mkdir -p $(ARCDIR); \
		cd $(ARCDIR) && git clone $(PK_URL); \
	else \
		$(MSG3) "PK source is cached" $(EMSG); \
	fi
	@touch .$(subst .,,$@)

$(PK_T)-get-patch: .$(PK_T)-get-patch

.$(PK_T)-get-patch: .$(PK_T)-get
	@touch .$(subst .,,$@)

# Unpack packages
$(PK_T)-unpack: .$(PK_T)-unpack

.$(PK_T)-unpack: .$(PK_T)-get-patch
	@mkdir -p $(BLDDIR)
	@cd $(ARCDIR) && $(UNPACK_CMD)
	@touch .$(subst .,,$@)

# Apply patches
$(PK_T)-patch: .$(PK_T)-patch

.$(PK_T)-patch: .$(PK_T)-unpack
	@if [ -d $(DIR_PATCH) ]; then \
		$(MSG) "================================================================"; \
		$(MSG2) "Patching source" $(EMSG); \
		$(MSG) "================================================================"; \
		for patchname in `ls -1 $(DIR_PATCH)/*.patch`; do \
			$(MSG3) Applying $$patchname $(EMSG); \
			cd $(PK_SRCDIR) && patch -Np1 -r - < $$patchname; \
		done; \
	fi
	@touch .$(subst .,,$@)

$(PK_T)-init: .$(PK_T)-init 

.$(PK_T)-init:
	@make build-verify
	@make .$(PK_T)-patch
	@mkdir -p $(PK_BLDDIR)/boot/overlays
	@mkdir -p $(PK_BLDDIR)/etc/X11/xorg.conf.d/
	@mkdir -p $(PK_BLDDIR)/usr/share/X11/xorg.conf.d/
	@mkdir -p $(PK_BLDDIR)/usr/share/lcdshow
	@mkdir -p $(PK_BLDDIR)/usr/bin
	@touch .$(subst .,,$@)

$(PK_T)-config: .$(PK_T)-config

.$(PK_T)-config:
	@touch .$(subst .,,$@)

$(PK_T): .$(PK_T)

.$(PK_T): .$(PK_T)-init 
	@make --no-print-directory $(PK_T)-config
	@$(MSG) "================================================================"
	@$(MSG2) "Building $(PK) for model $(MODEL)" $(EMSG)
	@$(MSG) "================================================================"
	@make --no-print-directory $(MODEL)
	@touch .$(subst .,,$@)

# ---------------------------------------------------------------
# Model specific builds
# These just setup the trees for the opkg building.
# ---------------------------------------------------------------
LCD35:
	@$(MSG3) "Adding overlays" $(EMSG)
	@cp $(PK_SRCDIR)/usr/tft35a-overlay.dtb $(PK_BLDDIR)/boot/overlays/
	@cp $(PK_SRCDIR)/usr/tft35a-overlay.dtb $(PK_BLDDIR)/boot/overlays/tft35a.dtbo
	@$(MSG3) "Adding MODEL.txt" $(EMSG)
	@cp $(SRCDIR)/config/$(MODEL).txt $(PK_BLDDIR)/usr/share/lcdshow/
	@$(MSG3) "Updating xorg config" $(EMSG)
	@cp -rf $(PK_SRCDIR)/usr/99-calibration.conf-35-90 $(PK_BLDDIR)/etc/X11/xorg.conf.d/99-calibration.conf
	@$(MSG3) "Installing tools" $(EMSG)
	@cp -f $(SCRIPTDIR)/* $(PK_BLDDIR)/usr/bin/
	@chmod +x $(PK_BLDDIR)/usr/bin/*
	@$(MSG3) "Rotation updates not yet supported." $(EMSG)

# ---------------------------------------------------------------
# Now package it
# ---------------------------------------------------------------
$(PK_T)-files:

# Package it as an opkg 
pkg $(PK_T)-pkg: .$(PK_T) 
	@make --no-print-directory root-verify opkg-verify
	@make --no-print-directory $(MODEL)-pkg

# ---------------------------------------------------------------
# Model specific packaging
# ---------------------------------------------------------------
LCD35-pkg:
	@mkdir -p $(PKGDIR)/opkg/$(PK)/CONTROL
	@mkdir -p $(PKGDIR)/opkg/$(PK)/media/mmcblk0p1
	@mkdir -p $(PKGDIR)/opkg/$(PK)/usr/share/lcdshow
	@mkdir -p $(PKGDIR)/opkg/$(PK)/etc/X11/xorg.conf.d
	@cp -r $(PK_BLDDIR)/etc $(PKGDIR)/opkg/$(PK)/
	@cp -r $(PK_BLDDIR)/usr $(PKGDIR)/opkg/$(PK)/
	@cp -r $(PK_BLDDIR)/boot/* $(PKGDIR)/opkg/$(PK)/media/mmcblk0p1
	@cp $(SRCDIR)/calibration/$(MODEL).normal $(PKGDIR)/opkg/$(PK)/etc/pointercal.normal
	@cp $(SRCDIR)/calibration/$(MODEL).rotate $(PKGDIR)/opkg/$(PK)/etc/pointercal.rotate
	@cp $(SRCDIR)/config/xorg.conf.hdmi $(PKGDIR)/opkg/$(PK)/etc/X11/xorg.conf
	@cp $(SRCDIR)/config/xorg.conf.* $(PKGDIR)/opkg/$(PK)/usr/share/lcdshow/
	@cp $(SRCDIR)/opkg/control $(PKGDIR)/opkg/$(PK)/CONTROL/control
	@cp $(SRCDIR)/opkg/debian-binary $(PKGDIR)/opkg/$(PK)/CONTROL/debian-binary
	@cp $(SRCDIR)/opkg/preinst $(PKGDIR)/opkg/$(PK)/CONTROL/preinst
	@cp $(SRCDIR)/opkg/postinst $(PKGDIR)/opkg/$(PK)/CONTROL/postinst
	@cp $(SRCDIR)/opkg/prerm $(PKGDIR)/opkg/$(PK)/CONTROL/prerm
	@cp $(SRCDIR)/opkg/postrm $(PKGDIR)/opkg/$(PK)/CONTROL/postrm
	@sed -i 's%\[MODEL\]%$(MODEL)%g' $(PKGDIR)/opkg/$(PK)/CONTROL/postinst
	@sed -i 's%\[MODEL\]%$(MODEL)%g' $(PKGDIR)/opkg/$(PK)/CONTROL/preinst
	@sed -i 's%\[MODEL\]%$(MODEL)%g' $(PKGDIR)/opkg/$(PK)/CONTROL/prerm
	@sed -i 's%\[MODEL\]%$(MODEL)%g' $(PKGDIR)/opkg/$(PK)/CONTROL/postrm
	@sed -i 's%\[PROJ\]%$(PK)%g' $(PKGDIR)/opkg/$(PK)/CONTROL/control
	@sed -i 's%\[MODEL\]%$(MODEL)%g' $(PKGDIR)/opkg/$(PK)/CONTROL/control
	@sed -i 's%\[VERSION\]%$(BLD_VERSION)%g' $(PKGDIR)/opkg/$(PK)/CONTROL/control
	@chmod +x $(PKGDIR)/opkg/$(PK)/CONTROL/*
	@chown -R root:root $(PKGDIR)/opkg/$(PK)/
	@cd $(PKGDIR)/opkg/ && $(OPKG_DIR)/opkg-build -O $(PK)
	@cp $(PKGDIR)/opkg/*.opk $(PKGDIR)/
	@rm -rf $(PKGDIR)/opkg

# ---------------------------------------------------------------
# Cleanup of builds
# ---------------------------------------------------------------

# Clean the packaging
pkg-clean $(PK_T)-pkg-clean:
	@if [ "$(PKGDIR)" != "" ] && [ -d "$(PKGDIR)" ]; then rm -rf $(PKGDIR); fi

# Clean out the build tree, but not the source tree
$(PK_T)-clean: $(PK_T)-pkg-clean
	@if [ "$(PK_BLDDIR)" != "" ] && [ -d "$(PK_BLDDIR)" ]; then rm -rf $(PK_BLDDIR); fi
	@rm -f .$(PK_T)-config .$(PK_T)-init .$(PK_T)-patch .$(PK_T) 

# Clean out everything associated with PK except the archive (only a top level clobber does that)
$(PK_T)-clobber: root-verify pkg-clean $(PK_T)-clean
	@if [ "$(PK_SRCDIR)" != "" ] && [ -d "$(PK_SRCDIR)" ]; then rm -rf $(PK_SRCDIR); fi
	@rm -f .$(PK_T)-unpack .$(PK_T)-get-patch .$(PK_T)-get 

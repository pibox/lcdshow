#!/bin/sh
# Touchscreen calibration and testing for PiBox.
# ----------------------------------------------------------------------
#--------------------------------------------------------------
# Initialization
#--------------------------------------------------------------
DO_TSCALIBRATE=0
DO_TSTEST=0
DO_XICALIBRATE=0
DEVNAME="Touchscreen"

#--------------------------------------------------------------
# Functions
#--------------------------------------------------------------
# Provide command line usage assistance
doHelp()
{
    echo ""
    echo "$0 [-ctx | -d devname]"
    echo "where"
    echo "-c          Run ts-calibrate."
    echo "-t          Run ts-test."
    echo "-x          Run xinput-calibrator"
    echo "-d devname  Specify devname to search for"
    echo "            Default: ${DEVNAME}"
    echo ""
    echo "Notes:"
    echo "xinput-calibrator won't work with PiBox.  PiBox relies"
    echo "on tslib for touchscreen management. So skip the"
    echo "xinput-calibrator test."
}

#--------------------------------------------------------------
# Read command line arguments
#--------------------------------------------------------------
while getopts ":ctxd:" Option
do
    case $Option in
    d) DEVNAME=${OPTARG};;
    c) DO_TSCALIBRATE=1;;
    t) DO_TSTEST=1;;
    x) DO_XICALIBRATE=1;;
    *) doHelp; exit 0;;
    esac
done

# Setup the environment for the tests.
export TSLIB_FBDEVICE=/dev/fb1
export TSLIB_TSDEVICE="$(pbeventlist | grep ${DEVNAME} | cut -f1 -d":")"
export DISPLAY=:0.0

if [[ ${DO_TSCALIBRATE} -eq 1 ]]; then
	ts_calibrate
	exit 0
fi

if [[ ${DO_TSTEST} -eq 1 ]]; then
	ts_test
	exit 0
fi

if [[ ${DO_XICALIBRATE} -eq 1 ]]; then
	xinput_calibrator
	exit 0
fi

